#include <algorithm>
#include <cassert>
#include <cstdarg>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <functional>
#include <memory>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <vector>

#undef UNICODE
#undef _UNICODE
#include <windows.h>

#include "GL/glcorearb.h"
#include "GL/glext.h"
#include "GL/wglext.h"

#define XR_DISABLE_COPYING(typeName)                                           \
  typeName(const typeName&) = delete;                                          \
  typeName& operator=(const typeName&) = delete

#define XR_DEFAULT_MOVE(typeName)                                              \
  typeName(typeName&&) = default;                                              \
  typeName& operator=(typeName&&) = default;

namespace xrLowLevel {

struct CompatibleDCDeleter {
  void operator()(HDC compatibleDC) const noexcept { DeleteDC(compatibleDC); }
};

struct HDCDeleter {
  HWND _window;
  HDCDeleter() noexcept = default;
  explicit HDCDeleter(const HWND wnd) noexcept : _window{wnd} {}

  void operator()(HDC dc) const noexcept { ReleaseDC(_window, dc); }
};

struct HGLRCDeleter {
  HDC _windowDC;
  HGLRCDeleter() noexcept = default;
  explicit HGLRCDeleter(const HDC windowDC) noexcept : _windowDC{windowDC} {}

  void operator()(HGLRC openglContext) const noexcept {
    wglMakeCurrent(_windowDC, nullptr);
    if (openglContext)
      wglDeleteContext(openglContext);
  }
};

struct HBITMAPDeleter {
  void operator()(HBITMAP bitmap) const noexcept { DeleteObject(bitmap); }
};

struct HFONTDeleter {
  void operator()(HFONT font) const noexcept { DeleteObject(font); }
};

void debugOutputString(const char*   file,
                       const int32_t line,
                       const char*   fmt,
                       ...) {
  char tmpBuff[1024];
  _snprintf(tmpBuff, _countof(tmpBuff), "\n[%s : %d]\n", file, line);
  OutputDebugString(tmpBuff);
  fputs(tmpBuff, stderr);

  va_list argsPtr;
  va_start(argsPtr, fmt);
  _vsnprintf(tmpBuff, _countof(tmpBuff), fmt, argsPtr);
  va_end(argsPtr);

  OutputDebugString(tmpBuff);
  fputs(tmpBuff, stderr);

  fflush(stderr);
}

POINT getPrimaryMonitorResolution() {
  auto primaryMonitor = MonitorFromPoint({0, 0}, MONITOR_DEFAULTTOPRIMARY);
  if (!primaryMonitor)
    return {1024, 1024};

  MONITORINFO monInfo;
  monInfo.cbSize = sizeof(monInfo);
  if (!GetMonitorInfo(primaryMonitor, &monInfo)) {
    return {1024, 1024};
  }

  return {std::abs(monInfo.rcWork.right - monInfo.rcWork.left),
          std::abs(monInfo.rcWork.bottom - monInfo.rcWork.top)};
}

} // namespace xrLowLevel

#define DEBUG_PRINTF(msg, ...)                                                 \
  do {                                                                         \
    xrLowLevel::debugOutputString(__FILE__, __LINE__, msg, ##__VA_ARGS__);     \
  } while (0)

///
/// Pointers to WGL extensions
struct wgl {
  static PFNWGLCHOOSEPIXELFORMATARBPROC    ChoosePixelFormatARB;
  static PFNWGLCREATECONTEXTATTRIBSARBPROC CreateContextAttribsARB;
  static PFNWGLSWAPINTERVALEXTPROC         SwapIntervalEXT;
};

PFNWGLCHOOSEPIXELFORMATARBPROC    wgl::ChoosePixelFormatARB;
PFNWGLCREATECONTEXTATTRIBSARBPROC wgl::CreateContextAttribsARB;
PFNWGLSWAPINTERVALEXTPROC         wgl::SwapIntervalEXT;

///
/// Pointers to OpenGL functions
struct gl {
#define GL_FNPROTO(FuncPtr, Name) static FuncPtr Name;
#include "opengl_prototypes.inl"
#undef GL_FNPROTO

  static bool loadFunctions();
};

#define GL_FNPROTO(FuncPtr, Name) FuncPtr gl::Name;
#include "opengl_prototypes.inl"
#undef GL_FNPROTO

bool gl::loadFunctions() {
#define GL_FNPROTO(FuncPtr, Name)                                              \
  do {                                                                         \
    assert(gl::Name == nullptr);                                               \
    gl::Name = (FuncPtr) wglGetProcAddress("gl" #Name);                        \
    if (!gl::Name) {                                                           \
      DEBUG_PRINTF("Failed to load function %s", "gl" #Name);                  \
      return false;                                                            \
    }                                                                          \
  } while (0);

#include "opengl_prototypes.inl"
#undef GL_FNPROTO

  return true;
}

using xrScopedWindowDC =
    std::unique_ptr<std::remove_pointer<HDC>::type, xrLowLevel::HDCDeleter>;

using xrScopedOpenGLContext =
    std::unique_ptr<std::remove_pointer<HGLRC>::type, xrLowLevel::HGLRCDeleter>;

using xrScopedHFont =
    std::unique_ptr<std::remove_pointer<HFONT>::type, xrLowLevel::HFONTDeleter>;

using xrScopedHBitmap = std::unique_ptr<std::remove_pointer<HBITMAP>::type,
                                        xrLowLevel::HBITMAPDeleter>;

using xrScopedCompatibleDC = std::unique_ptr<std::remove_pointer<HDC>::type,
                                             xrLowLevel::CompatibleDCDeleter>;

template <typename HandleDeleter>
struct xrGlHandle {
public:
  xrGlHandle() noexcept = default;
  explicit xrGlHandle(const GLuint glResource) noexcept
      : _glResource{glResource} {}

  ~xrGlHandle() noexcept {
    if (_glResource)
      HandleDeleter{}(_glResource);
  }

  xrGlHandle(xrGlHandle<HandleDeleter>&& rval) noexcept {
    std::swap(_glResource, rval._glResource);
  }

  xrGlHandle<HandleDeleter>&
  operator=(xrGlHandle<HandleDeleter>&& rval) noexcept {
    std::swap(_glResource, rval._glResource);
    return *this;
  }

  explicit operator bool() const noexcept { return _glResource != 0; }

  template <typename Dp>
  friend GLuint rawHandle(const xrGlHandle<Dp>& glh) noexcept;

  template <typename Dp>
  friend GLuint* rawHandlePtr(xrGlHandle<Dp>& glh) noexcept;

private:
  GLuint _glResource{};

  xrGlHandle(const xrGlHandle<HandleDeleter>&) = delete;
  xrGlHandle<HandleDeleter>&
  operator=(const xrGlHandle<HandleDeleter>&) = delete;
};

template <typename Dp>
GLuint rawHandle(const xrGlHandle<Dp>& glh) noexcept {
  return glh._glResource;
}

template <typename Dp>
GLuint* rawHandlePtr(xrGlHandle<Dp>& glh) noexcept {
  assert(glh._glResource == 0);
  return &glh._glResource;
}

struct xrGlBufferDeleter {
  void operator()(GLuint buff) const noexcept { gl::DeleteBuffers(1, &buff); }
};

using xrGlScopedBuffer = xrGlHandle<xrGlBufferDeleter>;

struct xrGlVertexArrayDeleter {
  void operator()(GLuint vao) const noexcept {
    gl::DeleteVertexArrays(1, &vao);
  }
};

using xrGlScopedVertexArray = xrGlHandle<xrGlVertexArrayDeleter>;

struct xrGlShaderDeleter {
  void operator()(GLuint shader) const noexcept { gl::DeleteShader(shader); }
};

using xrGlScopedShader = xrGlHandle<xrGlShaderDeleter>;

struct xrGlProgramDeleter {
  void operator()(GLuint program) const noexcept { gl::DeleteProgram(program); }
};

using xrGlScopedProgram = xrGlHandle<xrGlProgramDeleter>;

struct xrGlPipelineDeleter {
  void operator()(GLuint pipeline) const noexcept {
    gl::DeleteProgramPipelines(1, &pipeline);
  }
};

using xrGlScopedProgramPipeline = xrGlHandle<xrGlPipelineDeleter>;

struct xrGlTextureDeleter {
  void operator()(GLuint texture) const noexcept {
    gl::DeleteTextures(1, &texture);
  }
};

using xrGlScopedTexture = xrGlHandle<xrGlTextureDeleter>;

struct xrGlSamplerDeleter {
  void operator()(GLuint sampler) const noexcept {
    gl::DeleteSamplers(1, &sampler);
  }
};

using xrGlScopedSampler = xrGlHandle<xrGlSamplerDeleter>;

class xrBasicWindow {
public:
  xrBasicWindow();

  explicit operator bool() const noexcept { return _wndHandle != nullptr; }

  void messageLoop();

  struct {
    std::function<void()>      Draw;
    std::function<void(float)> Update;
  } Events;

  HDC deviceContext() const noexcept { return _wndDC.get(); }

private:
  void init();

private:
  static LRESULT WINAPI windowProcStub(HWND, UINT, WPARAM, LPARAM);
  LRESULT windowProc(UINT msg, WPARAM wparam, LPARAM lparam);
  void drawFrame();

  static LRESULT WINAPI dummyWindowProc(HWND, UINT, WPARAM, WPARAM);

private:
  bool                  _shouldClose{false};
  int32_t               _pixelFormatId{};
  HWND                  _wndHandle{nullptr};
  xrScopedWindowDC      _wndDC{};
  xrScopedOpenGLContext _openGLContex{};

  xrBasicWindow(const xrBasicWindow&) = delete;
  xrBasicWindow& operator=(const xrBasicWindow&) = delete;
};

void xrBasicWindow::init() {

  if (_pixelFormatId == 0) {
    constexpr const char* const kTempWindowClassName =
        "__##@@_TempOpenGLWindow";

    const WNDCLASSEX wndClass = {
        sizeof(wndClass),
        CS_OWNDC,
        DefWindowProc,
        0,
        0,
        GetModuleHandle(nullptr),
        LoadIcon(nullptr, IDI_APPLICATION),
        LoadCursor(nullptr, IDC_ARROW),
        (HBRUSH) GetStockObject(COLOR_WINDOW + 1),
        nullptr,
        kTempWindowClassName,
        nullptr,
    };

    //
    // First call, so just create a dummy window and a dummy context to
    // get a suitable pixel format
    RegisterClassEx(&wndClass);
    HWND tempWindow = CreateWindowEx(0L,
                                     kTempWindowClassName,
                                     "TempOpenGL Window",
                                     WS_OVERLAPPEDWINDOW,
                                     CW_USEDEFAULT,
                                     CW_USEDEFAULT,
                                     CW_USEDEFAULT,
                                     CW_USEDEFAULT,
                                     nullptr,
                                     nullptr,
                                     GetModuleHandle(nullptr),
                                     this);

    if (!tempWindow) {
      DEBUG_PRINTF("Failed to create temporary window!!");
      return;
    }

    {
      xrScopedWindowDC devContext{GetDC(tempWindow),
                                  xrLowLevel::HDCDeleter{tempWindow}};

      const auto pfd = []() {
        PIXELFORMATDESCRIPTOR pfd = {0};
        pfd.nSize                 = sizeof(PIXELFORMATDESCRIPTOR);
        pfd.nVersion              = 1;
        pfd.dwFlags =
            PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
        pfd.iPixelType   = PFD_TYPE_RGBA;
        pfd.cColorBits   = 32;
        pfd.cDepthBits   = 24;
        pfd.cStencilBits = 8;
        pfd.iLayerType   = PFD_MAIN_PLANE;

        return pfd;
      }();

      const auto pixelFormat = ChoosePixelFormat(devContext.get(), &pfd);
      if (pixelFormat == 0) {
        DEBUG_PRINTF("Failed to find suitable pixel format!");
        return;
      }

      if (!SetPixelFormat(devContext.get(), pixelFormat, &pfd)) {
        DEBUG_PRINTF("Failed to set new pixel format for temporary window!");
        return;
      }

      xrScopedOpenGLContext tempGLContext{wglCreateContext(devContext.get())};
      if (!tempGLContext) {
        DEBUG_PRINTF("Failed to create initial OpenGL context!");
        return;
      }

      wglMakeCurrent(devContext.get(), tempGLContext.get());

      wgl::ChoosePixelFormatARB =
          (PFNWGLCHOOSEPIXELFORMATARBPROC) wglGetProcAddress(
              "wglChoosePixelFormatARB");

      if (!wgl::ChoosePixelFormatARB) {
        DEBUG_PRINTF("wglChoosePixelFormatARB not present!");
        return;
      }

      const int32_t desiredPixelFmtProperties[] = {WGL_DRAW_TO_WINDOW_ARB,
                                                   TRUE,
                                                   /* Full acceleration */
                                                   WGL_ACCELERATION_ARB,
                                                   WGL_FULL_ACCELERATION_ARB,
                                                   /* OpenGL support */
                                                   WGL_SUPPORT_OPENGL_ARB,
                                                   TRUE,
                                                   /* Double buffered */
                                                   WGL_DOUBLE_BUFFER_ARB,
                                                   TRUE,
                                                   WGL_PIXEL_TYPE_ARB,
                                                   WGL_TYPE_RGBA_ARB,
                                                   WGL_COLOR_BITS_ARB,
                                                   32,
                                                   WGL_RED_BITS_ARB,
                                                   8,
                                                   WGL_GREEN_BITS_ARB,
                                                   8,
                                                   WGL_BLUE_BITS_ARB,
                                                   8,
                                                   WGL_ALPHA_BITS_ARB,
                                                   8,
                                                   WGL_DEPTH_BITS_ARB,
                                                   24,
                                                   WGL_STENCIL_BITS_ARB,
                                                   8,
                                                   0};

      uint32_t numFormats{};
      wgl::ChoosePixelFormatARB(devContext.get(),
                                desiredPixelFmtProperties,
                                nullptr,
                                1,
                                &_pixelFormatId,
                                &numFormats);

      if (numFormats == 0) {
        DEBUG_PRINTF("wglChoosePixelFormat found no suitable pixel format!");
        return;
      }
    }

    DestroyWindow(tempWindow);
    //
    //  Found a suitable pixel format, call again
    init();
    return;
  }

  assert(_pixelFormatId != 0);
  //
  // Since we have found a suitable pixel format, we will now create
  // the real window, where we will draw stuff.
  constexpr const char* const kWindowClassName = "___@@##_TEMPWGLWindow";

  const WNDCLASSEX wndClass = {
      sizeof(wndClass),
      CS_OWNDC,
      &xrBasicWindow::windowProcStub,
      0,
      0,
      GetModuleHandle(nullptr),
      LoadIcon(nullptr, IDI_APPLICATION),
      LoadCursor(nullptr, IDC_ARROW),
      (HBRUSH) GetStockObject(COLOR_WINDOW + 1),
      nullptr,
      kWindowClassName,
      nullptr,
  };

  RegisterClassEx(&wndClass);

  constexpr auto kWindowStyle = WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
  const auto     monSize      = xrLowLevel::getPrimaryMonitorResolution();
  RECT           windowGeom{0, 0, monSize.x, monSize.y};
  AdjustWindowRectEx(&windowGeom, kWindowStyle, false, 0L);

  _wndHandle = CreateWindowEx(0,
                              kWindowClassName,
                              "WGL Demo",
                              kWindowStyle,
                              windowGeom.left,
                              windowGeom.top,
                              windowGeom.right - windowGeom.left,
                              windowGeom.bottom - windowGeom.top,
                              nullptr,
                              nullptr,
                              GetModuleHandle(nullptr),
                              this);
  if (!_wndHandle) {
    DEBUG_PRINTF("Failed to recreate window !");
    return;
  }

  _wndDC =
      xrScopedWindowDC{GetDC(_wndHandle), xrLowLevel::HDCDeleter{_wndHandle}};

  PIXELFORMATDESCRIPTOR selectedPixelFormat;
  if (!DescribePixelFormat(_wndDC.get(),
                           _pixelFormatId,
                           (UINT) sizeof(selectedPixelFormat),
                           &selectedPixelFormat)) {
    DEBUG_PRINTF("Failed to get description of pixel format %d",
                 _pixelFormatId);
    return;
  }

  //
  //  set the new pixel format
  if (!SetPixelFormat(_wndDC.get(), _pixelFormatId, &selectedPixelFormat)) {
    DEBUG_PRINTF("Failed to set chosen pixel format!");
    return;
  }

  //
  //  Create another temp context
  {
    xrScopedOpenGLContext tempGLContext{wglCreateContext(_wndDC.get())};
    if (!tempGLContext) {
      DEBUG_PRINTF("Failed to create temp OpenGL context (2)");
      return;
    }

    wglMakeCurrent(_wndDC.get(), tempGLContext.get());

    //
    // load wglCreateContextAttribsARB
    wgl::CreateContextAttribsARB =
        (PFNWGLCREATECONTEXTATTRIBSARBPROC) wglGetProcAddress(
            "wglCreateContextAttribsARB");
    if (!wgl::CreateContextAttribsARB) {
      DEBUG_PRINTF("wglCreateContextAttribsARB not found!");
      return;
    }

    //
    //  Finally we can now create the real context
    const int32_t contextAttribs[] = {WGL_CONTEXT_MAJOR_VERSION_ARB,
                                      4,
                                      WGL_CONTEXT_MINOR_VERSION_ARB,
                                      5,
                                      WGL_CONTEXT_FLAGS_ARB,
                                      WGL_CONTEXT_DEBUG_BIT_ARB,
                                      WGL_CONTEXT_PROFILE_MASK_ARB,
                                      WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
                                      0};

    _openGLContex = xrScopedOpenGLContext{
        wgl::CreateContextAttribsARB(_wndDC.get(), nullptr, contextAttribs),
        xrLowLevel::HGLRCDeleter{_wndDC.get()}};

    if (!_openGLContex) {
      DEBUG_PRINTF("Failed to create REAL OPENGL context!");
      return;
    }
  }

  //
  //  Make new context current
  wglMakeCurrent(_wndDC.get(), _openGLContex.get());
  //
  // Load OpenGL functions
  if (!gl::loadFunctions()) {
    DEBUG_PRINTF("Failed to load OpenGL functions");
  }

  ShowWindow(_wndHandle, SW_SHOWNORMAL);
  UpdateWindow(_wndHandle);
}

xrBasicWindow::xrBasicWindow() { init(); }

LRESULT WINAPI xrBasicWindow::windowProcStub(HWND   wnd,
                                             UINT   msg,
                                             WPARAM wparam,
                                             LPARAM lparam) {
  if (msg == WM_CREATE) {
    auto wndObjPtr = (xrBasicWindow*) ((CREATESTRUCT*) lparam)->lpCreateParams;
    SetWindowLongPtr(wnd, GWLP_USERDATA, (LONG_PTR) wndObjPtr);
    return true;
  }

  auto wndPtrObj =
      (xrBasicWindow*) (LONG_PTR) GetWindowLongPtr(wnd, GWLP_USERDATA);
  if (!wndPtrObj) {
    return DefWindowProc(wnd, msg, wparam, lparam);
  }

  return wndPtrObj->windowProc(msg, wparam, lparam);
}

LRESULT xrBasicWindow::windowProc(UINT msg, WPARAM wparam, LPARAM lparam) {
  bool msgHandled{true};

  switch (msg) {
  case WM_CLOSE:
    DestroyWindow(_wndHandle);
    break;

  case WM_DESTROY:
    PostQuitMessage(0);
    break;

  default:
    msgHandled = false;
    break;
  }

  if (!msgHandled)
    return DefWindowProc(_wndHandle, msg, wparam, lparam);

  return 0L;
}

void xrBasicWindow::messageLoop() {
  while (!_shouldClose) {
    MSG wndMsg;

    while (PeekMessage(&wndMsg, nullptr, 0, 0, PM_NOREMOVE) == TRUE) {
      const auto result = GetMessage(&wndMsg, nullptr, 0, 0);
      if (result <= 0 || wndMsg.message == WM_QUIT) {
        _shouldClose = true;
        break;
      }
      TranslateMessage(&wndMsg);
      DispatchMessage(&wndMsg);
    }

    if (Events.Update)
      Events.Update(0.0f);

    drawFrame();
  }
}

void xrBasicWindow::drawFrame() {
  if (Events.Draw) {
    Events.Draw();
  } else {
    gl::ClearColor(0.0f, 0.2f, 0.5f, 1.0f);
    gl::Clear(GL_COLOR_BUFFER_BIT);
  }
  SwapBuffers(_wndDC.get());
}

struct xrMathConsts {
  template <typename T>
  static constexpr T Pi = T{3.14159265359f};

  template <typename T>
  static constexpr T TwoPi = T{2.0f} * T{3.14159265359f};
};

template <typename T>
struct xrScalar2 {
  union {
    struct {
      T x;
      T y;
    };
    T components[2];
  };

  xrScalar2() noexcept = default;
  constexpr xrScalar2(T x_, T y_) noexcept : x{x_}, y{y_} {}
};

using xrVec2     = xrScalar2<float>;
using xrUI16Vec2 = xrScalar2<uint16_t>;

template <typename T>
struct xrScalar3 {
  union {
    struct {
      T x;
      T y;
      T z;
    };
    T components[3];
  };

  xrScalar3() noexcept = default;
  constexpr xrScalar3(T x_, T y_, T z_) noexcept : x{x_}, y{y_}, z{z_} {}
};

using xrVec3     = xrScalar3<float>;
using xrUI8Vec3  = xrScalar3<uint8_t>;
using xrUI16Vec3 = xrScalar3<uint16_t>;

template <typename T>
struct xrScalar3X3 {
  union {
    struct {
      T a00, a01, a02;
      T a10, a11, a12;
      T a20, a21, a22;
    };

    T components[9];
  };

  xrScalar3X3() noexcept = default;
  constexpr xrScalar3X3(const T a00_,
                        const T a01_,
                        const T a02_,
                        const T a10_,
                        const T a11_,
                        const T a12_,
                        const T a20_,
                        const T a21_,
                        const T a22_) noexcept
      : a00{a00_}
      , a01{a01_}
      , a02{a02_}
      , a10{a10_}
      , a11{a11_}
      , a12{a12_}
      , a20{a20_}
      , a21{a21_}
      , a22{a22_} {}
};

using xrMat3 = xrScalar3X3<float>;

struct xrTransformsR3 {
  template <typename T>
  static xrScalar3X3<T> rotate(const T angleRads) noexcept;
};

template <typename T>
xrScalar3X3<T> xrTransformsR3::rotate(const T angleRads) noexcept {
  const auto st = std::sin(angleRads);
  const auto ct = std::cos(angleRads);

  return {ct, -st, T{}, st, ct, T{}, T{}, T{}, T{1}};
}

struct xrVertexPC {
  xrVec3 pos;
  xrVec3 color;
};

xrGlScopedProgram xrMakeProgram(const GLenum type,
                                const char** srcStrings,
                                const size_t numStrings) {
  xrGlScopedProgram program{
      gl::CreateShaderProgramv(type, (GLsizei) numStrings, srcStrings)};

  if (!program) {
    return xrGlScopedProgram{};
  }

  GLint linkStatus{GL_FALSE};
  gl::GetProgramiv(rawHandle(program), GL_LINK_STATUS, &linkStatus);
  if (linkStatus == GL_TRUE) {
    return program;
  }

  char  tmpBuff[1024];
  GLint logLen{};
  gl::GetProgramInfoLog(
      rawHandle(program), (GLsizei) _countof(tmpBuff), &logLen, tmpBuff);
  tmpBuff[logLen] = 0;

  DEBUG_PRINTF("[Link/compile error] %s", tmpBuff);
  return xrGlScopedProgram{};
}

class xrSimpleDemo {
public:
  xrSimpleDemo();

  explicit operator bool() const noexcept {
    return _vertexBuffer && _vertexArray && _vertexPrg && _fragPrg &&
           _programPipeline;
  }

  void drawStuff();
  void update(const float deltaTm);

private:
  xrGlScopedBuffer          _vertexBuffer;
  xrGlScopedVertexArray     _vertexArray;
  xrGlScopedProgram         _vertexPrg;
  xrGlScopedProgram         _fragPrg;
  xrGlScopedProgramPipeline _programPipeline;
  xrGlScopedTexture         _objMaterial;
  xrGlScopedSampler         _texSampler;
  float                     _rotAngle{};
  GLint                     _uniformTfMtxLoc{-1};
  GLint                     _uniformTexMtlLoc{-1};
};

xrSimpleDemo::xrSimpleDemo() {
  constexpr xrVertexPC kTriangleVertices[] = {
      {{-0.8f, -0.8f, 0.0f}, {1.0f, 0.0f, 0.0f}},
      {{0.0f, +0.8f, 0.0f}, {0.0f, 1.0f, 0.0f}},
      {{+0.8f, -0.8f, 0.0f}, {0.0f, 0.0f, 1.0f}}};

  gl::CreateBuffers(1, rawHandlePtr(_vertexBuffer));
  gl::NamedBufferStorage(rawHandle(_vertexBuffer),
                         (GLsizei) sizeof(kTriangleVertices),
                         kTriangleVertices,
                         0);

  gl::CreateVertexArrays(1, rawHandlePtr(_vertexArray));
  gl::VertexArrayVertexBuffer(rawHandle(_vertexArray),
                              0,
                              rawHandle(_vertexBuffer),
                              0,
                              (GLsizei) sizeof(xrVertexPC));
  gl::VertexArrayAttribFormat(rawHandle(_vertexArray),
                              0,
                              3,
                              GL_FLOAT,
                              GL_FALSE,
                              (GLuint) offsetof(xrVertexPC, pos));
  gl::VertexArrayAttribFormat(rawHandle(_vertexArray),
                              1,
                              3,
                              GL_FLOAT,
                              GL_FALSE,
                              (GLuint) offsetof(xrVertexPC, color));
  gl::EnableVertexArrayAttrib(rawHandle(_vertexArray), 0);
  gl::EnableVertexArrayAttrib(rawHandle(_vertexArray), 1);
  gl::VertexArrayAttribBinding(rawHandle(_vertexArray), 0, 0);
  gl::VertexArrayAttribBinding(rawHandle(_vertexArray), 1, 0);

  const char* kVertexShaderCode =
      "#version 450 core\n"
      "   layout(location = 0) in vec3 vsinPos;\n"
      "   layout(location = 1) in vec3 vsinColor;\n"
      "   out gl_PerVertex {\n"
      "     vec4 gl_Position;\n"
      "   };\n"
      "   out VS_OUT_PS_IN { \n"
      "     layout(location = 0) vec3 color;\n"
      "     layout(location = 1) vec2 texcoords;\n"
      "   } vsOutPsIn;\n"
      "\n"
      "uniform mat3 TransformMtx;\n"
      "void main() {\n"
      "   gl_Position = vec4(TransformMtx * vsinPos, 1.0);\n"
      "   vsOutPsIn.color = vsinColor;\n"
      "   vsOutPsIn.texcoords = vsinPos.xy;\n"
      "}\n";

  _vertexPrg = xrMakeProgram(GL_VERTEX_SHADER, &kVertexShaderCode, 1);
  if (!_vertexPrg) {
    DEBUG_PRINTF("Failed to create vertex program!");
    return;
  }

  _uniformTfMtxLoc =
      gl::GetUniformLocation(rawHandle(_vertexPrg), "TransformMtx");
  assert(_uniformTfMtxLoc != -1);

  const char* kFragmentShadeCode =
      "#version 450 core\n"
      "in VS_OUT_PS_IN {\n"
      "   layout(location = 0) vec3 color;\n"
      "   layout(location = 1) vec2 texcoords;\n"
      "} psIn;\n"
      "layout(location = 0) out vec4 fragColor;\n"
      "layout(binding = 0) uniform sampler2D TextureMtl;\n"
      "void main() {\n"
      "   fragColor = texture(TextureMtl, psIn.texcoords);\n"
      "}\n";

  _fragPrg = xrMakeProgram(GL_FRAGMENT_SHADER, &kFragmentShadeCode, 1);
  if (!_fragPrg) {
    DEBUG_PRINTF("Failed to create fragment program!");
    return;
  }

  gl::CreateProgramPipelines(1, rawHandlePtr(_programPipeline));

  using namespace std;
  vector<xrUI8Vec3> texPixels{512 * 512};

  for (uint32_t y = 0; y < 512; ++y) {
    for (uint32_t x = 0; x < 512; ++x) {
      auto imgPixel = &texPixels[y * 512 + x];
      imgPixel->x   = x % 255;
      imgPixel->y   = y % 255;
    }
  }

  gl::CreateTextures(GL_TEXTURE_2D, 1, rawHandlePtr(_objMaterial));
  gl::TextureStorage2D(rawHandle(_objMaterial), 1, GL_RGBA8, 512, 512);
  gl::TextureSubImage2D(rawHandle(_objMaterial),
                        0,
                        0,
                        0,
                        512,
                        512,
                        GL_RGB,
                        GL_UNSIGNED_BYTE,
                        &texPixels[0]);

  gl::CreateSamplers(1, rawHandlePtr(_texSampler));
  gl::SamplerParameteri(
      rawHandle(_texSampler), GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  gl::SamplerParameteri(
      rawHandle(_texSampler), GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void xrSimpleDemo::drawStuff() {
  gl::ClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  gl::Clear(GL_COLOR_BUFFER_BIT);
  gl::BindVertexArray(rawHandle(_vertexArray));
  gl::UseProgramStages(
      rawHandle(_programPipeline), GL_VERTEX_SHADER_BIT, rawHandle(_vertexPrg));
  gl::UseProgramStages(
      rawHandle(_programPipeline), GL_FRAGMENT_SHADER_BIT, rawHandle(_fragPrg));

  const auto tfMatrix = xrTransformsR3::rotate(_rotAngle);
  gl::ProgramUniformMatrix3fv(
      rawHandle(_vertexPrg), _uniformTfMtxLoc, 1, GL_TRUE, tfMatrix.components);

  gl::BindTextureUnit(0, rawHandle(_objMaterial));
  gl::BindSampler(0, rawHandle(_texSampler));
  gl::ProgramUniform1i(rawHandle(_fragPrg), 0, 0);

  gl::BindProgramPipeline(rawHandle(_programPipeline));
  gl::DrawArrays(GL_TRIANGLES, 0, 3);
}

void xrSimpleDemo::update(const float /*deltaTm*/) {
  _rotAngle += 0.01f;
  if (_rotAngle >= xrMathConsts::TwoPi<float>) {
    _rotAngle -= xrMathConsts::TwoPi<float>;
  }
}

struct xrReadOnlyMemoryMappedFile {
public:
  xrReadOnlyMemoryMappedFile() noexcept = default;
  xrReadOnlyMemoryMappedFile(const char* filePath) noexcept;
  ~xrReadOnlyMemoryMappedFile() noexcept;

  xrReadOnlyMemoryMappedFile(xrReadOnlyMemoryMappedFile&& rval) noexcept {
    std::swap(_fileHandle, rval._fileHandle);
    std::swap(_fileMapping, rval._fileMapping);
    std::swap(_mappingView, rval._mappingView);
  }

  xrReadOnlyMemoryMappedFile&
  operator=(xrReadOnlyMemoryMappedFile&& rval) noexcept {
    std::swap(_fileHandle, rval._fileHandle);
    std::swap(_fileMapping, rval._fileMapping);
    std::swap(_mappingView, rval._mappingView);

    return *this;
  }

  explicit operator bool() const noexcept { return _mappingView != nullptr; }
  const void* data() const noexcept { return _mappingView; }

  HANDLE fileHandle() const noexcept { return _fileHandle; }

private:
  HANDLE _fileHandle{INVALID_HANDLE_VALUE};
  HANDLE _fileMapping{nullptr};
  void*  _mappingView{nullptr};

  XR_DISABLE_COPYING(xrReadOnlyMemoryMappedFile);
};

xrReadOnlyMemoryMappedFile::xrReadOnlyMemoryMappedFile(
    const char* filePath) noexcept {
  _fileHandle = CreateFile(filePath,
                           GENERIC_READ,
                           FILE_SHARE_READ,
                           nullptr,
                           OPEN_EXISTING,
                           FILE_ATTRIBUTE_NORMAL,
                           nullptr);

  if (_fileHandle == INVALID_HANDLE_VALUE) {
    DEBUG_PRINTF("Failed to open file %s, error %u", filePath, GetLastError());
    return;
  }

  _fileMapping =
      CreateFileMapping(_fileHandle, nullptr, PAGE_READONLY, 0, 0, nullptr);

  if (_fileMapping == nullptr) {
    DEBUG_PRINTF("Failed to create file mapping, error %u", GetLastError());
    return;
  }

  _mappingView = MapViewOfFile(_fileMapping, FILE_MAP_READ, 0, 0, 0);

  if (_mappingView == nullptr) {
    DEBUG_PRINTF("Failed to map view of file, error %u", GetLastError());
  }
}

xrReadOnlyMemoryMappedFile::~xrReadOnlyMemoryMappedFile() noexcept {
  if (_mappingView) {
    UnmapViewOfFile(_mappingView);
  }

  if (_fileMapping) {
    CloseHandle(_fileMapping);
  }

  if (_fileHandle != INVALID_HANDLE_VALUE) {
    CloseHandle(_fileHandle);
  }
}

struct xrGlyph {
  uint8_t glyphSheetIdx;
  float   glyphWidth;
};

struct xrGlyphSheet {
  xrGlyphSheet() = default;

  int16_t                    glyphHeight{};
  int16_t                    glyphAscent{};
  int16_t                    glyphDescent{};
  int16_t                    glyphExternalLeading{};
  int16_t                    glyphBaseline{};
  std::unique_ptr<xrGlyph[]> glyphInfoTable;
  std::unique_ptr<float>     glyphKerningTable;
  std::unique_ptr<xrVec2[]>  glyphTexcoords;

  XR_DISABLE_COPYING(xrGlyphSheet);
  XR_DEFAULT_MOVE(xrGlyphSheet);
};

struct xrFont {
  using xrFontGlyphsTableType = std::unordered_map<uint16_t, xrGlyphSheet>;

  xrScopedHBitmap                fontRenderBitmap;
  xrFontGlyphsTableType          fontGlyphSheetsBySize;
  std::vector<xrGlScopedTexture> fontGlyphSheets;
  xrUI16Vec2                     fontGlyphSheetNextFreePos{0, 0};
  HDC                            fontRenderDC;

  xrFont() = default;
  XR_DISABLE_COPYING(xrFont);
  XR_DEFAULT_MOVE(xrFont);
};

class xrFontEngine {
public:
  explicit xrFontEngine(HDC deviceContext);
  XR_DEFAULT_MOVE(xrFontEngine);

public:
  bool loadFont(const char* fontFile, float fontSize);
  void drawGlyphSheet();

private:
  HDC _deviceContext;
  std::unordered_map<std::string, xrFont> _fontsTable;
  xrGlScopedBuffer          _vertexBufferCodepoints;
  xrGlScopedBuffer          _indexBufferCodepoints;
  xrGlScopedVertexArray     _vertexArray;
  xrGlScopedProgram         _vertexShader;
  xrGlScopedProgram         _fragmentShader;
  xrGlScopedProgramPipeline _programPipeline;
  xrGlScopedSampler         _glyphSheetSampler;
  GLint                     _uniformGlyphColorLoc{-1};
  uint32_t                  _activeGlyphSheet{};

private:
  XR_DISABLE_COPYING(xrFontEngine);
};

xrFontEngine::xrFontEngine(HDC deviceContext) : _deviceContext{deviceContext} {
  const xrVec2 quadPoints[] = {
      {-1.0f, -1.0f}, {-1.0f, 1.0f}, {1.0f, 1.0f}, {1.0f, -1.0f}};
  const uint8_t indices[] = {0, 1, 2, 0, 2, 3};

  gl::CreateBuffers(1, rawHandlePtr(_vertexBufferCodepoints));
  gl::NamedBufferStorage(rawHandle(_vertexBufferCodepoints),
                         (GLsizei) sizeof(quadPoints),
                         quadPoints,
                         0);

  gl::CreateBuffers(1, rawHandlePtr(_indexBufferCodepoints));
  gl::NamedBufferStorage(
      rawHandle(_indexBufferCodepoints), (GLsizei) sizeof(indices), indices, 0);

  gl::CreateVertexArrays(1, rawHandlePtr(_vertexArray));
  gl::VertexArrayVertexBuffer(rawHandle(_vertexArray),
                              0,
                              rawHandle(_vertexBufferCodepoints),
                              0,
                              (GLsizei) sizeof(xrVec2));
  gl::VertexArrayElementBuffer(rawHandle(_vertexArray),
                               rawHandle(_indexBufferCodepoints));
  gl::VertexArrayAttribFormat(
      rawHandle(_vertexArray), 0, 2, GL_FLOAT, GL_FALSE, 0);
  gl::VertexArrayAttribBinding(rawHandle(_vertexArray), 0, 0);
  gl::EnableVertexArrayAttrib(rawHandle(_vertexArray), 0);

  constexpr const char* kVertexShaderCode =
      "#version 450 core\n"
      "layout (location = 0) in vec2 vsInPos;\n"
      "\n"
      "out gl_PerVertex {\n"
      "   vec4 gl_Position;\n"
      "};\n"
      "\n"
      "void main() {\n"
      "   gl_Position = vec4(vsInPos, 0.0f, 1.0f);\n"
      "}";

  _vertexShader =
      xrMakeProgram(GL_VERTEX_SHADER, (const char**) &kVertexShaderCode, 1);

  constexpr char* const kFragmentShaderCode =
      "version 450 core\n"
      "layout (binding = 0) uniform sampler2D GlyphSheet;\n"
      "uniform vec3 GlyphColor;\n"
      "layout (location = 0) out vec4 fragColor;\n"
      "\n"
      "void main(void) {\n"
      "   const vec2 texCoords = vec2(gl_FragCoord.xy);\n"
      "   fragColor = vec4(texture(GlyphSheet, texCoords).rgb * GlyphColor, "
      "1.0);\n"
      "}";

  _fragmentShader =
      xrMakeProgram(GL_FRAGMENT_SHADER, (const char**) &kFragmentShaderCode, 1);

  gl::CreateProgramPipelines(1, rawHandlePtr(_programPipeline));
  gl::CreateSamplers(1, rawHandlePtr(_glyphSheetSampler));
  gl::SamplerParameteri(
      rawHandle(_glyphSheetSampler), GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  gl::SamplerParameteri(
      rawHandle(_glyphSheetSampler), GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  _uniformGlyphColorLoc =
      gl::GetUniformLocation(rawHandle(_fragmentShader), "GlyphColor");
  assert(_uniformGlyphColorLoc != -1);

  gl::UseProgramStages(rawHandle(_programPipeline),
                       GL_VERTEX_SHADER_BIT,
                       rawHandle(_vertexShader));
  gl::UseProgramStages(rawHandle(_programPipeline),
                       GL_FRAGMENT_SHADER_BIT,
                       rawHandle(_fragmentShader));
}

bool xrFontEngine::loadFont(const char* fontFile, float fontSize) {
  xrFont* fontTableEntry = &_fontsTable[fontFile];

  if (!fontTableEntry->fontRenderBitmap) {
    fontTableEntry->fontRenderBitmap =
        xrScopedHBitmap{CreateCompatibleBitmap(_deviceContext, 1024, 1024)};
    SelectObject(_deviceContext, fontTableEntry->fontRenderBitmap.get());
    RECT bitmapRectangle{0, 0, 1024, 1024};
    FillRect(
        _deviceContext, &bitmapRectangle, (HBRUSH) GetStockObject(BLACK_BRUSH));
  }

  SelectObject(_deviceContext, fontTableEntry->fontRenderBitmap.get());

  xrReadOnlyMemoryMappedFile mmappedFont{fontFile};
  if (!mmappedFont) {
    DEBUG_PRINTF("Failed to open font file %s", fontFile);
    return false;
  }

  LARGE_INTEGER fileSize;
  if (!GetFileSizeEx(mmappedFont.fileHandle(), &fileSize)) {
    return false;
  }

  DWORD installedFonts{};
  auto  fontResourceHandle = AddFontMemResourceEx(
      (void*) mmappedFont.data(), fileSize.LowPart, nullptr, &installedFonts);

  if (!fontResourceHandle || !installedFonts) {
    DEBUG_PRINTF("Failed to load font!");
    return false;
  }

  DEBUG_PRINTF("Loaded %u fonts", installedFonts);

  xrScopedHFont loadedFont{CreateFont((int32_t) fontSize,
                                      0,
                                      0,
                                      0,
                                      FW_NORMAL,
                                      FALSE,
                                      FALSE,
                                      FALSE,
                                      ANSI_CHARSET,
                                      OUT_OUTLINE_PRECIS,
                                      CLIP_DEFAULT_PRECIS,
                                      ANTIALIASED_QUALITY,
                                      FF_DONTCARE,
                                      nullptr)};

  if (!loadedFont) {
    DEBUG_PRINTF("Failed to create font!");
    return false;
  }

  auto fontHandle = loadedFont.get();

  using namespace std;

  auto originalBitmap =
      SelectObject(_deviceContext, fontTableEntry->fontRenderBitmap.get());

  SelectObject(_deviceContext, fontHandle);
  SetTextColor(_deviceContext, RGB(255, 255, 255));
  SetBkColor(_deviceContext, RGB(0, 0, 0));
  SetBkMode(_deviceContext, OPAQUE);

  const auto numBytes = GetOutlineTextMetrics(_deviceContext, 0, nullptr);
  assert(numBytes != 0);

  unique_ptr<uint8_t, decltype(&free)> textOutlineData{
      (uint8_t*) malloc(numBytes), &free};

  if (!GetOutlineTextMetrics(_deviceContext,
                             numBytes,
                             (OUTLINETEXTMETRIC*) textOutlineData.get())) {
    DEBUG_PRINTF("Failed to get text ouytline metrics !");
    return false;
  }

  const OUTLINETEXTMETRIC* textMetricPtr =
      (OUTLINETEXTMETRIC*) textOutlineData.get();

  static constexpr auto kNumCodePoints = 255 - 32;

  xrGlyphSheet newSheet{};
  auto         glyphSheet        = &newSheet;
  const auto   kerningPairsCount = GetKerningPairs(_deviceContext, 0, nullptr);

  if (kerningPairsCount != 0) {
    vector<KERNINGPAIR> kerningTable{kerningPairsCount};
    GetKerningPairs(_deviceContext, kerningPairsCount, &kerningTable[0]);

    auto itrEndPairs = remove_if(
        begin(kerningTable), end(kerningTable), [](const KERNINGPAIR& kp) {
          return kp.wFirst < 32 || kp.wFirst > 255 || kp.wSecond < 32 ||
                 kp.wSecond > 255;
        });

    glyphSheet->glyphKerningTable.reset(
        new float[kNumCodePoints * kNumCodePoints]);
    memset(glyphSheet->glyphKerningTable.get(), 0, sizeof(float));

    for_each(
        begin(kerningTable), itrEndPairs, [glyphSheet](const auto& kernPair) {
          auto kernTblPtr = glyphSheet->glyphKerningTable.get();
          kernTblPtr[kernPair.wFirst * kNumCodePoints + kernPair.wSecond] =
              (float) kernPair.iKernAmount;
        });
  }

  glyphSheet->glyphHeight  = (int16_t) textMetricPtr->otmTextMetrics.tmHeight;
  glyphSheet->glyphAscent  = (int16_t) textMetricPtr->otmTextMetrics.tmAscent;
  glyphSheet->glyphDescent = (int16_t) textMetricPtr->otmTextMetrics.tmDescent;
  glyphSheet->glyphExternalLeading =
      (int16_t) textMetricPtr->otmTextMetrics.tmExternalLeading;
  glyphSheet->glyphBaseline =
      (int16_t)(textMetricPtr->otmMacAscent -
                textMetricPtr->otmTextMetrics.tmInternalLeading);

  glyphSheet->glyphInfoTable.reset(new xrGlyph[kNumCodePoints]);
  glyphSheet->glyphTexcoords.reset(new xrVec2[kNumCodePoints]);

  uint32_t glyphsRendered{};
  uint32_t glyphTextureSheetIdx{
      (uint32_t) fontTableEntry->fontGlyphSheets.size()};

  bool dirtyTexture = (fontTableEntry->fontGlyphSheetNextFreePos.x != 0) ||
                      (fontTableEntry->fontGlyphSheetNextFreePos.y != 0);

  auto copyGlyphsToTexture = [
    originalBitmap,
    &dirtyTexture,
    fontTableEntry,
    deviceContext = _deviceContext
  ](const bool clearBitmap) {
    GdiFlush();
    SelectObject(deviceContext, originalBitmap);

    BITMAPINFO bmi       = {0};
    bmi.bmiHeader.biSize = sizeof(bmi.bmiHeader);

    auto retCode = GetDIBits(deviceContext,
                             fontTableEntry->fontRenderBitmap.get(),
                             0,
                             0,
                             nullptr,
                             &bmi,
                             DIB_RGB_COLORS);

    assert(retCode == TRUE);
    assert(bmi.bmiHeader.biCompression == BI_BITFIELDS);

    const auto                  imgBytes = bmi.bmiHeader.biSizeImage;
    alignas(BITMAPINFO) uint8_t bmpInfoBuff[sizeof(bmi) + 3 * sizeof(DWORD)];
    auto                        pbmp = (BITMAPINFO*) &bmpInfoBuff[0];

    memcpy(pbmp, &bmi, sizeof(bmi));
    unique_ptr<void, decltype(&free)> bitmapBits{malloc(imgBytes), &free};

    retCode = GetDIBits(deviceContext,
                        fontTableEntry->fontRenderBitmap.get(),
                        0,
                        std::abs(bmi.bmiHeader.biHeight),
                        bitmapBits.get(),
                        pbmp,
                        DIB_RGB_COLORS);

    assert(retCode != FALSE);

    if (!dirtyTexture) {
      xrGlScopedTexture glyphsTexture;
      gl::CreateTextures(GL_TEXTURE_2D, 1, rawHandlePtr(glyphsTexture));
      gl::TextureStorage2D(rawHandle(glyphsTexture), 1, GL_RGB8, 1024, 1024);
      fontTableEntry->fontGlyphSheets.push_back(move(glyphsTexture));
    } else {
      dirtyTexture = false;
    }

    assert(!fontTableEntry->fontGlyphSheets.empty());
    const auto textureId = rawHandle(fontTableEntry->fontGlyphSheets.back());
    gl::TextureSubImage2D(textureId,
                          0,
                          0,
                          0,
                          1024,
                          1024,
                          GL_RGB,
                          GL_UNSIGNED_BYTE,
                          bitmapBits.get());

    SelectObject(deviceContext, fontTableEntry->fontRenderBitmap.get());
    if (clearBitmap) {
      RECT bitmapRC{0, 0, 1024, 1024};
      FillRect(deviceContext, &bitmapRC, (HBRUSH) GetStockObject(BLACK_BRUSH));
    }
  };

  for (uint8_t ch = 32; ch < 255; ++ch) {
    uint32_t glyphIdx = ch - 32;

    SIZE glyphSize;
    GetTextExtentPoint32(_deviceContext, (char*) &ch, 1, &glyphSize);

    if ((fontTableEntry->fontGlyphSheetNextFreePos.x + glyphSize.cx) > 1024) {
      fontTableEntry->fontGlyphSheetNextFreePos.x = 0;
      fontTableEntry->fontGlyphSheetNextFreePos.y +=
          glyphSheet->glyphHeight + glyphSheet->glyphExternalLeading;

      if ((fontTableEntry->fontGlyphSheetNextFreePos.y +
           glyphSheet->glyphHeight + glyphSheet->glyphExternalLeading) > 1024) {
        copyGlyphsToTexture(true);

        glyphsRendered                              = ch - 32;
        fontTableEntry->fontGlyphSheetNextFreePos.y = 0;
        glyphTextureSheetIdx =
            (uint32_t) fontTableEntry->fontGlyphSheets.size();
      }
    }

    glyphSheet->glyphInfoTable[glyphIdx] =
        xrGlyph{(uint8_t) glyphTextureSheetIdx, (float) glyphSize.cx};
    glyphSheet->glyphTexcoords[glyphIdx] = xrVec2{
        (float) fontTableEntry->fontGlyphSheetNextFreePos.x / (float) 1024,
        (float) fontTableEntry->fontGlyphSheetNextFreePos.y / (float) 1024};

    const char tmpStr[] = {(char) ch, 0};
    ExtTextOut(_deviceContext,
               (int32_t) fontTableEntry->fontGlyphSheetNextFreePos.x,
               (int32_t) fontTableEntry->fontGlyphSheetNextFreePos.y,
               ETO_OPAQUE,
               nullptr,
               tmpStr,
               1,
               nullptr);

    fontTableEntry->fontGlyphSheetNextFreePos.x += (uint16_t) glyphSize.cx;
  }

  if (glyphsRendered != kNumCodePoints) {
    copyGlyphsToTexture(false);
  }

  fontTableEntry->fontGlyphSheetsBySize[(uint16_t) fontSize] = move(newSheet);

  return true;
}

void xrFontEngine::drawGlyphSheet() {
  gl::BindVertexArray(rawHandle(_vertexArray));
  gl::BindSampler(0, rawHandle(_glyphSheetSampler));

  auto activeFontPtr = &std::begin(_fontsTable)->second;
  gl::BindTextureUnit(
      0, rawHandle(activeFontPtr->fontGlyphSheets[_activeGlyphSheet]));

  const xrVec3 kFontColor{1.0f, 1.0f, 0.0f};
  gl::ProgramUniform3fv(rawHandle(_fragmentShader),
                        _uniformGlyphColorLoc,
                        1,
                        kFontColor.components);
  gl::UseProgram(0);
  gl::BindProgramPipeline(rawHandle(_))
  //   HDC _deviceContext;
  // std::unordered_map<std::string, xrFont> _fontsTable;
  // xrGlScopedBuffer          _vertexBufferCodepoints;
  // xrGlScopedBuffer          _indexBufferCodepoints;
  // xrGlScopedVertexArray     _vertexArray;
  // xrGlScopedProgram         _vertexShader;
  // xrGlScopedProgram         _fragmentShader;
  // xrGlScopedProgramPipeline _programPipeline;
  // xrGlScopedSampler         _glyphSheetSampler;
  // GLint                     _uniformGlyphColorLoc{-1};
  // uint32_t                  _activeGlyphSheet{};
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {

  xrBasicWindow mainWindow;
  if (!mainWindow) {
    DEBUG_PRINTF("Failed to initialize window !");
    return EXIT_FAILURE;
  }

  xrFontEngine fontEngine{mainWindow.deviceContext()};

  fontEngine.loadFont("c:/windows/fonts/arial.ttf", 20);
  fontEngine.loadFont("c:/windows/fonts/arial.ttf", 120);

  xrSimpleDemo simpleDemo{};
  if (!simpleDemo) {
    DEBUG_PRINTF("Epic failure!");
    return EXIT_FAILURE;
  }

  mainWindow.Events.Update =
      std::bind(&xrSimpleDemo::update, &simpleDemo, std::placeholders::_1);
  mainWindow.Events.Draw = std::bind(&xrSimpleDemo::drawStuff, &simpleDemo);
  mainWindow.messageLoop();

  return EXIT_SUCCESS;
}